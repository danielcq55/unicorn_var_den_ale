V = VectorElement("CG", "tetrahedron", 1)
Q = FiniteElement("CG", "tetrahedron", 1)
Z = FiniteElement("DG", "tetrahedron", 0)
ZV = VectorElement("DG", "tetrahedron", 0)

v = TestFunction(V)
v2 = Coefficient(V)
u = Coefficient(V)
rho = Coefficient(Q)
up = Coefficient(V)
q = TestFunction(Q)
q2 = Coefficient(Q)
eta = TestFunction(Q)
p = Coefficient(Q)
nu = Coefficient(Q)
h = Coefficient(Z)
k = Coefficient(Z)
c0 = Coefficient(Z)
c1 = Coefficient(Z)
c2 = Coefficient(Z)
u0 = Coefficient(V)
rho0 = Coefficient(Q)
p0 = Coefficient(Q)
f = Coefficient(V)
hmin = Coefficient(Z)
umean = Coefficient(ZV)
cv = Coefficient(Z)
R_sc = Coefficient(Z)
psi_m = Coefficient(V)
psi_c = Coefficient(Q)
bpsi_c = Coefficient(Q)
phi_m = Coefficient(V)
phi_c = Coefficient(Q)
z = TestFunction(Z)
zz = Coefficient(Z)

#Mesh Velocity Variables
w0 = Coefficient(V)
w  = Coefficient(V)
wm = Coefficient(V)

#weak BDCs
sm = Coefficient(Z)
n = Coefficient(V)

u_ = TrialFunction(V)
rho_ = TrialFunction(Q)
p_ = TrialFunction(Q)

rho_i0 = Coefficient(Q)

icv = 1./cv
um = 0.5*(u + u0)
rhom = 0.5*(rho + rho0)

#Mesh Velocity Variable
wm = 0.5*(w + w0)


kk = 0.25 * hmin
#d = c1*h
#d = c1 * 0.5 / sqrt( 1.0 / kk**2 + inner(umean, umean) / h**2)
d = c0*h**(3./2.)
dd = h**2
dd3 = h**(3./2.)

rhoMax = 1.0
#rhoMin = rhoMax
rhoMin = 0.001

one = as_vector((1.0, 1.0, 1.0))

beta = (rhoMin - rhom)*(rhoMax - rhom)
gamma = abs(grad(rhom)[0]) + abs(grad(rhom)[1]) + abs(grad(rhom)[2])
beta2 = 0.2*1./(0.1*gamma + 1.)*beta

beta_rho = (rhoMin - rhom)*-rhom
gamma_rho = abs(grad(rhom)[0]) + abs(grad(rhom)[1]) + abs(grad(rhom)[2])
beta2_rho = 0.2*1./(0.1*gamma_rho + 1.)*beta_rho

beta_eta = (rhoMin - rhom)*-eta
gamma_eta = abs(grad(rhom)[0]) + abs(grad(rhom)[1]) + abs(grad(rhom)[2])
beta2_eta = 0.2*1./(0.1*gamma_eta + 1.)*beta_eta

R = [rhom*grad(um)*um - rhom*f, inner(um, grad(rhom)) + inner(one, grad(beta2_rho)), div(um)]
R_v = [rhom*grad(v)*um, inner(v, grad(rhom)), div(v)]
R_eta = [eta*grad(um)*um, inner(um, grad(eta)) + inner(one, grad(beta2_eta)), 0]
R_q = [grad(q), 0, 0]

LS_u = d*(inner(R[0], R_v[0]) + inner(R[1], R_v[1]) + inner(R[2], R_v[2]))  
LS_rho = d*(inner(R[0], R_eta[0]) + inner(R[1], R_eta[1]) + inner(R[2], R_eta[2]))  
LS_p = d*(inner(R[0] + grad(p), R_q[0]) + inner(R[1], R_q[1]) + inner(R[2], R_q[2]))

C_av = (abs(u[0]) + abs(u[1]) + abs(u[2]))

stab1 = c1
stab2 = c2
alpha = 1.0

AV_u = C_av*(stab1*dd*R_sc + stab2*dd3)*inner(grad(um), grad(v))
AV_rho = C_av*(stab1*dd*R_sc + stab2*dd3)*inner(grad(rhom), grad(eta))



# Weak residuals of variable-density incompressible model
#with mesh velocity wm
r_m = (abs(rhom)*inner(u - u0, v)/k + ((nu*inner(grad(um), grad(v)) + inner(grad(p) + rhom*grad(um)*(um-wm) - rhom*f, v))))*dx + LS_u*dx + AV_u*dx + sm*(1./h)*inner(um-wm, n)*inner(v, n)*ds
r_d = (inner(rho - rho0, eta)/k + (inner(um-wm, grad(rhom))*eta))*dx - beta2*inner(one, grad(eta))*dx + LS_rho*dx + AV_rho*dx



r_c = (alpha*inner(grad(p - p0), grad(q)) + inner(div(u), q))*dx + LS_p*dx

LR_sc = z*icv*abs((rho - rho0)/k + inner(um, grad(rhom)))*dx + \
         z*icv*abs((rhom*(u - u0)/k + grad(p) + R[0])[0])*dx + \
         z*icv*abs((rhom*(u - u0)/k + grad(p) + R[0])[1])*dx + \
         z*icv*abs((rhom*(u - u0)/k + grad(p) + R[0])[2])*dx


# Projection of initial condition for rho
aP_rho = inner(rho_, eta)*dx + stab1*dd*inner(grad(rho_), grad(eta))*dx
LP_rho = inner(rho_i0, eta)*dx
